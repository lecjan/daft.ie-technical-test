<?php

    // Including necessary PHP classes

    require_once('search_parser.php');
    require_once('search_results.php');
    
    // Defining an API key to access web services via SOAP and creating new SOAP client
    // All these to be used by both classes search_parser.php and search_results.php
    
    $api_key = "d737ed3bd1cdbd5a807588b3131b05903935c2fd";
    $DaftAPI = new SoapClient("http://api.daft.ie/v2/wsdl.xml");
    
?>


<!DOCTYPE html>
<!--
  -- This is a web aplication which is a subject to a technical test for DAFT.ie
  -- 
  -- author: Lech Jankowski
  --
  -- created 11/07-12/07/2016
  --
  -->



<html>
    <head>
        <meta charset="UTF-8">

        <title>DAFT.IE Technical Test</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">   
        
    </head>
    <body>

    <div class="container">
      <h1>DAFT.IE Technical Test</h1>
      <p>by Lech Jankowski.</p> 
        
        
        <!-- Search and Notes areas ------------------------------------------>
        
        <div class="panel-group">
            <div class="panel panel-success">
              
                <div class="panel-heading">Search Form</div>

                <div class="panel-body">

                    <div class="col-xs-6">
                        <label for="usr">Enter daft search query:</label>

                        <!-- creating a form to be submitted with the post method and send to same page itself -->

                        <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" > 
                            <div class="input-group">
                              <input name="search_text" type="text" size="40" class="form-control" id="search" >
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Search!</button>
                              </span>
                            </div><!-- /input-group -->                   
                        </form>

                    </div>
                </div>
            </div>

            <div class="panel panel-info">
              
                <div class="panel-heading">Search Notes</div>

                <div class="panel-body">

                    <h6>Valid keywords are:</h6>
                    <h6>[ad_type] like e.g. "shortterm", "sharing", "sale", "rental", "parking", "new_development", "commercial", "rent", "let"
                       <br>
                       [property_type] like e.g. "house", "apartment", "duplex", "bungalow", "site", "studio"
                       <br>
                       "[no. of bedrooms] bed" or "[no. of bedrooms] bedroom"
                       <br>
                       [area] like e.g. "Salthill", "Dublin 1"
                       <br>
                       <br>           
                       examples:
                       <br>
                       "2 bedroom apartment in Galway City Centre"
                       <br>
                       "2 bedroom apartment"
                       <br>
                       "5 bedroom house in Athenry"
                       <br>
                       "3 bedroom house"
                       <br>
                       "house for sale for 450000"
                       <br>
                       "house to let in Ardee"
                       <br>
                       "house sale for 250000 in Oranmore"
                       <br>
                        "5 bed rent"
                       <br>
                        "Castleknock 3 bedroom for sale"
                       <br>
                        "2 bed apartment to let Dublin 1"
                       <br>
                        "4 bed house to rent in Dundrum for 1000 per month"
                        <br> 
                        Note! "Dublin" area must contain its number e.g. "Dublin 2"
                    </h6>

                </div>
            </div>
        </div>        
        
        <!-- Results areas ---------------------------------------------------->
        
        <div class="panel panel-default">
            
            <div class="panel-heading">Search Results</div>
            
            <div class="panel-body">
                
                <?php

                    // Initiating text to avoid empty or null errors
                    $search_text_query = "";

                    // Check if the form has been submitted and process the POST variables

                    if ($_SERVER["REQUEST_METHOD"] == "POST"){

                        $search_text_query = $_POST["search_text"]; 

                        $errorText = ""; 

                        //check that the fields are not blank  

                        if ( empty ($search_text_query)  ){ 
                            $errorText .= " Please enter a search query text! "; 
                        } 

                        if ($errorText == ""){ 
                            //there are no errors so we are ok to process the submitted post

                            // display posted by the form text
                            echo "Entered search sentence: <strong>$search_text_query</strong><br/>";

                            // initiating new object of search_parser class
                            $search = new search_parser;
                            // calling a function to process text translation
                            $search->translate_query($search_text_query, $DaftAPI, $api_key);

                            // initiating new object of search_results class
                            $results = new search_results;
                            // calling a function to process printing out the results
                            // Note! function output requires $search object with set variables after translation
                            $results->output($search_text_query, $DaftAPI, $api_key, $search);                    

                        }else{ 
                            // if there are errors - reset text string
                            $search_text_query = "";

                            // show error message instead of results
                            echo "Oops... <strong>$errorText</strong><br/>";
                        } 
                    }         
                ?>
                
            </div>
        </div>        
        
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    
    </div>
    </body>
</html>
