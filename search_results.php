<?php

/**
 * Class search_results purpose is to produce the search and display of results
 * from DAFT API with given through $search object set of variables as parameters
 * of a request
 *
 * @author Lech Jankowski (D00161848)
 */

class search_results {

    /*
     * 
     * output function performs a request to DAFT API with given serach parameters
     * and produces an output of result data
     * 
     */
    
    public function output($search_text_query, $DaftAPI, $api_key, $search)
    {
        // starting from a first page of an output setting initial page number to be 1
        $page_num = 1;
        
        // looping through the result pages
        do {

            // setting DAFT API parameters
            $parameters = array(
                'api_key'   =>  $api_key
                , 'query'   =>  array('property_type' => $search->getPropertyType()
                                        ,'bedrooms' => $search->getNoBedroom()
                                        ,'areas' => array($search->getAreaId())
                                        ,'page' => $page_num
                                        ,'max_price' => $search->getMaxPrice()
                    )
            );

            // requesting proper response from API depending on given ad_type
            switch ($search->getAdType()) {
                case "shortterm":
                    $response = $DaftAPI->search_shortterm($parameters);
                    break;
                case "sharing":
                    $response = $DaftAPI->search_sharing($parameters);
                    break;
                case "parking":
                    $response = $DaftAPI->search_parking($parameters);
                    break;
                case "new_development":
                    $response = $DaftAPI->search_new_development($parameters);
                    break;
                case "commercial":
                    $response = $DaftAPI->search_commercial($parameters);
                    break;
                case "sale":
                    $response = $DaftAPI->search_sale($parameters);
                    break;
                case "rental":
                    $response = $DaftAPI->search_rental($parameters);
                    break;
                // deafault search is set to a "sale" when getAdType is equal ""
                default:
                    $response = $DaftAPI->search_sale($parameters);
            }                

            // assigning the results from response
            $results = $response->results;

            // displaying page headings
            echo "<br/><br/>";

            printf("DAFT.ie search sentence: ");
            echo "<strong>";
            printf($results->search_sentence);
            echo "</strong>";
            echo "<br/>";

            printf("Total results :");
            echo "<strong>";
            printf($results->pagination->total_results);
            echo "</strong>";                

            printf(", Number of pages :");
            echo "<strong>";
            printf($results->pagination->num_pages);
            echo "</strong>";                

            printf(", Current page :");
            echo "<strong>";
            printf($results->pagination->current_page);
            echo "</strong>";

            // process the results only if number of results are > 0
            if($results->pagination->total_results > 0)
            {
                // Calculating number of results per page
                // This number will be used to process the results
                // due to conditional different approach when there is 1 result or more

                $number_results_per_page = 0;
                if ($results->pagination->total_results <= 10)
                {
                        $number_results_per_page = $results->pagination->last_on_page;
                } else if($results->pagination->current_page > 1)
                    {
                        $number_results_per_page = $results->pagination->last_on_page - ($results->pagination->results_per_page*($page_num - 1));
                    } else
                    {
                        $number_results_per_page = $results->pagination->last_on_page;
                    }
                    
                // updating page heading    
                printf(", Results per page: ");
                echo "<strong>";
                printf($number_results_per_page);
                echo "</strong>";
                echo "<br/><br/>";

                // if number of results per page is > 1 process the results array
                // otherwise process single result object
                if ($number_results_per_page > 1)
                {
                    foreach($results->ads as $ad)
                    {
                        printf(
                            '<a href="%s">%s</a><br />'
                            , $ad->daft_url
                            , $ad->full_address
                        );
                    }
                } else 
                {
                    printf(
                        '<a href="%s">%s</a><br />'
                        , $results->ads->daft_url
                        , $results->ads->full_address
                    );                        
                }
            }

            // increase number of next page
            $page_num++;

        } while ($page_num <= $results->pagination->num_pages);  // finish when page_num is > that all pages count
        
    }
}
