<?php

/**
 * Class search_parser purpose is to set necessary variables to a values used for further searches parameters from DAFT API
 * Variables are accessed via getters.
 *
 * @author Lech Jankowski
 */

class search_parser {

    // declaration and initiation of a class variables
    
    private $_no_of_bedroom = "";
    private $_ad_type = "";
    private $_area_id = "";
    private $_property_type = "";
    private $_max_price = "";

    // setter funtions:
    
    /*
     * 
     *  translate_query function purpose is to translate or interpret given text in order 
     *  to substract and identify valid keywords and to set class variables   
     * 
     */
    
    public function translate_query($search_text_query, $DaftAPI, $api_key){

        /*
         * 
         *  Searching text for a "bedroom" keyword and identifying number of bedrooms.
         *  Key to finding no. of bedroom is a number 2 characters to the left of a "bedroom" keyword
         * 
         *  Note! Variant of alternative bedroom numbers e.g. "3 or 4" is not implemented here due to running out of time
         * 
         */
        
        // position of the string initiation
        $position = 0;
        
        // Finding the first occurence of a keyword "bed" or "bedroom"
        // If found process the finding number of bedrooms value
        if((strstr($search_text_query, "bed")) || (strstr($search_text_query, "bedroom")))
        {
            // finding first char position of a keyword
            $position = strpos($search_text_query,"bed");
            
            // assigning a value to a _no_of_bedrrom class variable with a character
            // assumed to be a number of bedrooms
            $this->_no_of_bedroom = substr($search_text_query, $position -2, 1);
        }        

        
        /*  
         * This will search for a type of the ad_types listed from API 
         * in the text and setting _ad_type class variable if found: 
         * 
         * shortterm
         * sharing
         * sale
         * rental
         * parking
         * new_development
         * commercial
         * 
         */

        // setting DAFT API parameters
        $parameters = array(
            'api_key'    => $api_key
        );

        // requesting the response from API
        $response = $DaftAPI->ad_types($parameters);

        // loop through response ad_types and assigning value to _ad_type
        // class variable if ad_type found in text
        foreach($response->ad_types as $ad_type)
        {
            if(strstr($search_text_query, $ad_type->name))
            {
                $this->_ad_type = $ad_type->name;
            }                     
        }

        // additional check for varied keywords may be used to describe "rental" ad type
        if((strstr($search_text_query, "rent")) || (strstr($search_text_query, "rental")) || (strstr($search_text_query, "let")))
        {
            $this->_ad_type = "rental";
        } 

        /**
         * 
         * Searching text for a property type:
         * house
         * apartment
         * duplex
         * bungalow
         * site
         * studio
         * 
         * This will search the text to find listed from API property types (short name)
         * If found - stores value of short name to _property_type class variable
         * 
         */

        // setting DAFT API parameters
        $parameters = array(
            'api_key'   =>  $api_key
            , 'ad_type' =>  "sale"
        );

        // requesting the response from API
        $response = $DaftAPI->property_types($parameters);

        // loop through response property_types and assigning value to _property_type
        // class variable if property_type found in text
        foreach($response->property_types as $property_type)
        {
            if(strstr($search_text_query, $property_type->short))
            {
                $this->_property_type = $property_type->short;
            }                    
        }


        /**
         * 
         *  Searching text for area:
         *  This will check if listed from API area was used in text,
         *  if yes, stores the value of area ID in a _area_id class variable
         * 
         */

        // setting DAFT API parameters
        $parameters = array(
            'api_key'       =>  $api_key
            , 'area_type'   =>  "area"
        );

        // requesting the response from API
        $response = $DaftAPI->areas($parameters);
        
        // loop through response areas and assigning value to class variable if area->name found in text
        foreach($response->areas as $area)
        {
            if(strstr($search_text_query, $area->name))
            {
                $this->_area_id = $area->id;
            }
        }
        
        
        /** 
         * 
         * Searching text for "for" keyword and identifying maximal price
         * Key to finding the price is a string number 4 characters to the right of "for" keyword
         * 
         */

        // position of the string initiation
        $position = 0;
        
        // if text contains "for" keyword process the seek related result
        if(strstr($search_text_query, "for"))
        {
            //Fixing the text if "for" keyword occures more that once
            //It may happen if "for sale for 320000" is given in text.
            // To fix it replace first "for sale" to "sale" or "for rent" to "rent"
            $search_text_query  = str_replace("for sale", "sale", $search_text_query);
            $search_text_query  = str_replace("for rent", "rent", $search_text_query);

            // find first position of a keyword
            $position = strpos($search_text_query,"for");

            // loop through next characters to cumulate the maximum price value
            // Key to this is assumption that next 4 chars to the right of "for" 
            // keyword is a numeric text which may be our max price
            
            // temp string
            $price_string = "";
            
            // temp character (supposed to be a numeric)
            $price_char = "";
            
            // initial offest of a character to parse
            $offset = 0;
            
            // looping through a text from 4 space right to "for" keyword
            do 
            {
                $price_char = substr($search_text_query, $position + 4 + $offset, 1);
                
                // checking if char is a numeric
                if( preg_match('([0-9])', $price_char) ) 
                { 
                    // char is numeric then add to a temp price string
                    $price_string .= $price_char;
                    // increase the offset
                    $offset++;
                } else {
                    // char was not a numeric then assuming numeric text (price) is finished. 
                    break; // end when next character after a number or "for" keyword is not a digit
                }
            } while (true); 
            
            // assign a value to class variable
            $this->_max_price = $price_string;
        }          

        

    }
    
    
    // getter functions to all class variables

    public function getNoBedroom()
    {
        return $this->_no_of_bedroom;
    }

    public function getAdType()
    {
        return $this->_ad_type;
    }

    public function getAreaId()
    {
        return $this->_area_id;
    }     

    public function getPropertyType()
    {
        return $this->_property_type;
    }              

    public function getMaxPrice()
    {
        return $this->_max_price;
    } 
    
}    
